var express = require('express');
var app     = express();
var config  = require('./config');
var path    = require('path');

app.use('/static', express.static(config.PUBLIC_DIRECTORY));

app.get('/*', function (req, res) {
  res.sendFile(config.indexFile(__dirname));
});

app.listen(config.PORT, function () {
  console.log('Semina, listening on: '+config.PORT);
});