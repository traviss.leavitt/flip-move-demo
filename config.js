module.exports = {

    PUBLIC_DIRECTORY: 'public',
    PORT: 3000,

    //helpers
    indexFile: function(dir) {
        return dir+'/'+this.PUBLIC_DIRECTORY+'/index.html';
    }

};