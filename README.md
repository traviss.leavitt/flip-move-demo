# FlipMove Demo
Demo illustrating animations with [FlipMove](https://github.com/joshwcomeau/react-flip-move#options)

# Setup
1. `npm install`
2. `gulp build`
3. Start the server `node server.js`