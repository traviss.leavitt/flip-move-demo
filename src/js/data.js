export default [
    'Apple', 'Orange', 'Grapefruit', 'Banana', 'Kiwi', 'Plum', 'Grape', 'Mango'
];

export default [
    {id: 1, name: 'Apple'},
    {id: 2, name: 'Orange'},
    {id: 3, name: 'Grapefruit'},
    {id: 4, name: 'Banana'},
    {id: 5, name: 'Kiwi'},
    {id: 6, name: 'Plum'},
    {id: 7, name: 'Grape'},
    {id: 8, name: 'Mango'}
]