import alt from '../alt';
import Actions from './actions';

import fruits from '../data';
import _ from 'lodash';

class Store {

    constructor() {
        this.fruits = [];
        this.bindActions(Actions);
    }

    onRemoveItem(id) {
        let index = _.findIndex(this.fruits, function(fruitItem) {
            return fruitItem.id == id;
        });

        if (index >= 0) {
            this.fruits.splice(index, 1);
        }
    }

    onSortFruits() {
        this.fruits.sort(function(a, b) {
            return a.name < b.name ? -1 : 1;
        });
    }

    onRandomize() {
        this.fruits = _.shuffle(this.fruits);
    }

    onResetFruits() {
        this.fruits = fruits.slice(0);
    }

}

export default alt.createStore(Store, 'Store');