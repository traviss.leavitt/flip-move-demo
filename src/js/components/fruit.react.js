import React from 'react';
import Actions from './actions';

class Fruit extends React.Component {

    handleClick(evt) {
        Actions.removeItem(this.props.id);
    }

    render() {
        return(
            <div className="fruit">
                {this.props.name}
                <button onClick={this.handleClick.bind(this)}>Remove</button>
            </div>
        );
    }

}

export default Fruit;