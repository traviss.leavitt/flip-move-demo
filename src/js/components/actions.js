import alt from '../alt';

class Actions {
    constructor() {
        this.generateActions(
            'removeItem', 'sortFruits', 'randomize', 'resetFruits'
        );
    }
}

export default alt.createActions(Actions);