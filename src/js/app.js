import React from 'react';
import ReactDOM from 'react-dom';
import alt from './alt';
import FlipMove from 'react-flip-move';

import fruits from './data';
import Store from './components/store';
import Actions from './components/actions';
import _ from 'lodash';

import Fruit from './components/fruit.react';

alt.bootstrap(JSON.stringify({
    Store: {
        fruits: fruits.slice(0)
    }
}));

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = Store.getState();
    }

    componentDidMount() {
        Store.listen(this.onChange.bind(this));
    }

    componentWillUnmount() {
        Store.unlisten(this.onChange.bind(this));
    }

    onChange(state) {
        this.setState(state);
    }

    handleSort(evt) {
        Actions.sortFruits();
    }

    handleRandomize(evt) {
        Actions.randomize();
    }

    handleResetFruits(evt) {
        Actions.resetFruits();
    }

    render() {
        let fruits = this.state.fruits.map((fruit, i) => {
            return(
                <Fruit key={fruit.id} {...fruit} />
            );
        });

        return(
            <div className="segment">
                <div className="fruits">
                    <FlipMove enterAnimation="elevator" leaveAnimation="elevator" typeName="div" staggerDurationBy="30" duration={400}>
                        {fruits}
                    </FlipMove>
                </div>
                <div className="controls">
                    <button onClick={this.handleSort.bind(this)}>Alphabetize</button>
                    <button onClick={this.handleRandomize.bind(this)}>Randomize</button>
                    <button onClick={this.handleResetFruits.bind(this)}>Reset</button>
                </div>
            </div>
        );
    }

}

ReactDOM.render(
    <App />, 
    document.getElementById('app')
);